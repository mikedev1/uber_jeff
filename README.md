# Uber_Jeff

## Production environment

PHP 8.2.0 (cli) (built: Dec  6 2022 15:31:23) (ZTS Visual C++ 2019 x64)

## Install node and npm in production && Symfony CLI

sudo service mysql start

CREATE USER 'phpmyadmin'@'localhost' IDENTIFIED BY 'some_pass';
GRANT ALL PRIVILEGES ON *.* TO 'phpmyadmin'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;

### Node

<https://medium.com/@iam_vinojan/how-to-install-node-js-and-npm-using-node-version-manager-nvm-143165b16ce1>

### Symfony CLI

<https://symfony.com/download>

## Ecouteur d evenements

### Loggin

<https://youtu.be/UKMwwU_5wJQ>

## Initialisation

`symfony server:run`
Wampserver Mysql 5.7.31
Symfony 6.3

## Coding standard

### Php fixer
 `tools/php-cs-fixer/vendor/bin/php-cs-fixer fix src`

### Eslint js

`npx eslint assets`

### eslint scss

`npx stylelint "assets/**/*.scss"`

add --fix for fixes errors

## Template

## Tailwind Css & Elements

<https://tailwind-elements.com/>

## Codepen

### Speed Dial

`https://codepen.io/daviglenn/pen/ZGdLbY`

### Float button

<https://codepen.io/alvarotrigo/pen/OJmrqVB>

<https://codepen.io/fosmont/pen/oNbOQWd>

### Faker

## Chargement de fausses données

`php bin/console doctrine:fixtures:load`

### Seo

<https://www.redacteur.com/blog/seo-balise-alt-images>

### Bundle

## Imagine filter

`php bin/console liip:imagine:cache:remove`

#### Resolve

`symfony console liip:imagine:cache:resolve /images/heart.jpg`

## Mysql Ubuntu
Reset root password

 `ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'new-password';`

## Stripe 

https://stripe.com/docs/checkout/quickstart#testing

4242 4242 4242 4242
Authentification requise pour le paiement
4000 0025 0000 3155
Paiement refusé
4000 0000 0000 9995

## Git 

Try:

git mergetool
It opens a GUI that steps you through each conflict, and you get to choose how to merge. Sometimes it requires a bit of hand editing afterwards, but usually it's enough by itself. It is much better than doing the whole thing by hand certainly.

As per Josh Glover's comment:

[This command] doesn't necessarily open a GUI unless you install one. Running git mergetool for me resulted in vimdiff being used. You can install one of the following tools to use it instead: meld, opendiff, kdiff3, tkdiff, xxdiff, tortoisemerge, gvimdiff, diffuse, ecmerge, p4merge, araxis, vimdiff, emerge.

Below is a sample procedure using vimdiff to resolve merge conflicts, based on this link.

Run the following commands in your terminal

git config merge.tool vimdiff
git config merge.conflictstyle diff3
git config mergetool.prompt false
This will set vimdiff as the default merge tool.

Run the following command in your terminal

git mergetool
You will see a vimdiff display in the following format:


Site de reservation 
Referencement google => Visibilité 
Lien notation google  
Bascule depuis les reseaux sociaux 
FaceBook // => lien à creer
carte de visite eventuellement // pro // =>blanc et vert, noir ecologigque // vehicule electrique ou hybride 
Bonne presentation // securite // service 
"Vous avez reservé un taxi? un chauffeur"
Reseaux de chauffeurs triés sur le volet => delegation de course commission à prendre pour Jeff
Reservation vtc taxi aeoroport beauvais reservation oise 

Monétique  stripe

Dans un premier temps reservation en ligne puis payable en ligne

nom / prenom / email / depart / arrivé / date / nb de personnes => forfait


Prix forfait

Beauvais aéroport/ paris 149€ 

Beauvais/ CDG  / 139€ 

Beauvais/ Orly / 189€ 

Beauvais/ Amiens / 119€ 

Beauvais/ Disneyland Paris / 199€

Prix forfait VAN

## Codepen

### Modal

https://codepen.io/f7deat/pen/JjROpPv
