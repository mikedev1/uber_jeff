// tailwind.config.js

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./templates/**/*.html.twig",
    "./node_modules/tw-elements/dist/js/**/*.js",
  ],
  
  plugins: [
    require("tw-elements/dist/plugin"),
    require("@tailwindcss/forms")
  ],
}
