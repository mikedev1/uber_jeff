<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241025135720 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE reset_password_request (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, selector VARCHAR(20) NOT NULL, hashed_token VARCHAR(100) NOT NULL, requested_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', expires_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_7CE748AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE contact ADD telephone VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE home_text ADD phrase_accroche LONGTEXT DEFAULT NULL, ADD titre_accroche VARCHAR(255) DEFAULT NULL, ADD subtitle_accroche VARCHAR(255) DEFAULT NULL, ADD titre_en VARCHAR(255) NOT NULL, ADD titre_es VARCHAR(255) NOT NULL, ADD titre_it VARCHAR(255) NOT NULL, ADD subtitle_en VARCHAR(255) NOT NULL, ADD subtitle_es VARCHAR(255) NOT NULL, ADD subtitle_it VARCHAR(255) NOT NULL, ADD slogan_en VARCHAR(255) NOT NULL, ADD slogan_es VARCHAR(255) NOT NULL, ADD slogan_it VARCHAR(255) NOT NULL, ADD phrase_accroche_en VARCHAR(255) NOT NULL, ADD phrase_accroche_es VARCHAR(255) NOT NULL, ADD phrase_accroche_it VARCHAR(255) NOT NULL, ADD titre_accroche_en VARCHAR(255) NOT NULL, ADD titre_accroche_es VARCHAR(255) NOT NULL, ADD titre_accroche_it VARCHAR(255) NOT NULL, ADD subtitle_accroche_en VARCHAR(255) NOT NULL, ADD subtitle_accroche_es VARCHAR(255) NOT NULL, ADD subtitle_accroche_it VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE reservation ADD retour_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD telephone VARCHAR(255) DEFAULT NULL, ADD pays VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE reset_password_request DROP FOREIGN KEY FK_7CE748AA76ED395');
        $this->addSql('DROP TABLE reset_password_request');
        $this->addSql('ALTER TABLE home_text DROP phrase_accroche, DROP titre_accroche, DROP subtitle_accroche, DROP titre_en, DROP titre_es, DROP titre_it, DROP subtitle_en, DROP subtitle_es, DROP subtitle_it, DROP slogan_en, DROP slogan_es, DROP slogan_it, DROP phrase_accroche_en, DROP phrase_accroche_es, DROP phrase_accroche_it, DROP titre_accroche_en, DROP titre_accroche_es, DROP titre_accroche_it, DROP subtitle_accroche_en, DROP subtitle_accroche_es, DROP subtitle_accroche_it');
        $this->addSql('ALTER TABLE user DROP telephone, DROP pays');
        $this->addSql('ALTER TABLE contact DROP telephone');
        $this->addSql('ALTER TABLE reservation DROP retour_at');
    }
}
