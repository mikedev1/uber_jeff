<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('objet', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez renseigner un objet',
                    ]),
                    new Length([
                        'min' => 3,
                        'max' => 50,
                        'minMessage' => '{{ limit }} caractères minimum',
                        'maxMessage' => '{{ limit }} caractères maximum',
                    ]),
                ],
            ])
            ->add('telephone', TelType::class, [
                'label' => 'Votre numéro de téléphone',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez renseigner votre numéro de téléphone',
                    ]),
                    new Length([
                        'min' => 4,
                        'max' => 20,
                        'minMessage' => '{{ limit }} caracteres minimum',
                        'maxMessage' => '{{ limit }} caractères maximum',
                    ]),
                ],
            ])
            ->add('email', EmailType::class, [
                'required' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez renseigner un email',
                    ]),
                ],
            ])
            ->add('contenu', TextareaType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez renseigner un message',
                    ]),
                    new Length([
                        'min' => 10,
                        'max' => 500,
                        'minMessage' => '{{ limit }} caracteres minimum',
                        'maxMessage' => '{{ limit }} caractères maximum',
                    ]),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}
