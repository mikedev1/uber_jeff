<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class DetailReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder

            ->add('createdAt', DateTimeType::class, [
                'required' => false,
                'label' => 'Jour de départ',
                'widget' => 'single_text',
                'attr' => [
                    'placeholder' => 'Jour de départ',
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez renseigner le jour de votre départ',
                    ]),
                    new Range([
                        'min' => '+12 hours',
                        'minMessage' => 'Les réservations sont possibles à partir du {{ limit }}',
                    ]),
                ],
            ])
            ->add('personne', NumberType::class, [
                'label' => 'Nombre de passagers',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Nombre de passagers',
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez renseigner le nombre de passagers',
                    ]),
                    new Range([
                        'min' => 1,
                        'max' => 4,
                        'notInRangeMessage' => 'Veuillez renseigner un nombre de passagers entre 1 et 4',
                    ]),
                ],
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Email',
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez renseigner votre email',
                    ]),
                ],
            ])
            ->add('telephone', TextType::class, [
                'label' => 'Votre numéro de téléphone',
                'required' => false,
                'attr' => [
                    'placeholder' => "Numéro de téléphone"
                ],
                'constraints' => [
                    new NotBlank([]),
                ]
            ])
            ->add('bagage', NumberType::class, [
                'label' => 'Nombre de bagages',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Nombre de bagages',
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez renseigner le nombre de bagages',
                    ]),
                    new Range([
                        'min' => 1,
                        'max' => 5,
                        'notInRangeMessage' => 'Veuillez renseigner un nombre de bagages entre 1 et 5',
                    ]),
                ],
            ])
            ->add('retour', ChoiceType::class, [

                'choices' => [
                    'Je profite de l\'offre' => true,
                ],
                'expanded' => true,
                'multiple' => true,
            ])

            ->add('retourAt', DateTimeType::class, [
                'required' => false,
                'label' => 'Votre retour',
                'widget' => 'single_text',
                'attr' => [
                    'placeholder' => 'Votre retour',
                ],
                'constraints' => [new Range([
                    'min' => '+13 hours',
                    'minMessage' => 'Les retours ne sont possibles qu\'à partir du {{ limit }}',
                ])]

            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
