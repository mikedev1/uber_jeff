<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', TextType::class, [
                'label' => 'Votre nom',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Votre nom',
                ],
                'constraints' => [
                    new NotBlank([]),
                    new Length([
                        'min' => 2,
                        'max' => 25
                    ])
                ]
            ])

            ->add('prenom' , TextType::class, [
                'label' => 'Votre prénom',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Votre prénom',
                ],
                'constraints' => [
                    new NotBlank([]),
                    new Length([
                        'min' => 2,
                        'max' => 25
                    ])
                ]
            ])

            ->add('pays',CountryType::class, [
                'invalid_message' => 'Veuillez sélectionner un pays valide',
                'label' => 'Pays',
                'preferred_choices' => ['muppets', 'arr'],
                'help' => 'Votre pays actuel' . ' '
                

            ])

            ->add('telephone' , TextType::class, [
                'label' => 'Votre numéro de téléphone',
                'required' => false,
                'help' => 'N\'oubliez pas d\'indiquer l\'indicatif téléphonique de votre pays',
                'attr' => [
                    'placeholder' => 'Votre numéro de téléphone',
                ],
                'constraints' => [
                    new NotBlank([]),
                    new Length([
                        'min' => 6,
                        'max' => 35
                    ])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
