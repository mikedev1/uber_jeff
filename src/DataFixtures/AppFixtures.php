<?php

namespace App\DataFixtures;

use App\Entity\Forfait;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function addForfait()
    {
        $forfait = new Forfait();

        $forfait
            ->setNom('Aeroport de Beauvais => Val d\'Oise')
            ->setPrix('150');

        return $forfait;
    }

    public function load(ObjectManager $manager): void
    {
        $plaintextPassword = 'fixtures';

        // hash the password (based on the security.yaml config for the $user class)

        $faker = Factory::create('fr_FR');

        // addForfait
        $forfait = $this->addForfait();

        $manager->persist($forfait);

        // Creation de 20 utilisateurs // hachage du mot de passe!
        for ($i = 0; $i < 20; ++$i) {
            $user = new User();

            $hashedPassword = $this->passwordHasher->hashPassword(
                $user,
                $plaintextPassword
            );

            $user->setNom($faker->lastName);
            $user->setPrenom($faker->firstName);
            $user->setEmail($faker->email());
            $user->setPassword($hashedPassword);
            $user->setRoles(['ROLE_USER']);

            $manager->persist($user);
        }

        $manager->flush();
    }
}
