<?php

namespace App\Entity;

use App\Repository\ForfaitRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[ORM\Entity(repositoryClass: ForfaitRepository::class)]
#[Vich\Uploadable]
class Forfait
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column]
    private ?float $prix = null;

    // NOTE: This is not a mapped field of entity metadata, just a simple property.
    #[Vich\UploadableField(mapping: 'forfaits', fileNameProperty: 'imageName', size: 'imageSize')]
    private ?File $imageFile = null;

    // NOTE: This is not a mapped field of entity metadata, just a simple property.
    #[Vich\UploadableField(mapping: 'forfaits', fileNameProperty: 'carousel0')]
    private ?File $carouselFile0 = null;

    // NOTE: This is not a mapped field of entity metadata, just a simple property.
    #[Vich\UploadableField(mapping: 'forfaits', fileNameProperty: 'carousel1')]
    private ?File $carouselFile1 = null;

    // NOTE: This is not a mapped field of entity metadata, just a simple property.
    #[Vich\UploadableField(mapping: 'forfaits', fileNameProperty: 'carousel2')]
    private ?File $carouselFile2 = null;

    #[ORM\OneToMany(mappedBy: 'forfait', targetEntity: Reservation::class, cascade: ['remove'])]
    private Collection $reservations;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $imageName = null;

    #[ORM\Column(nullable: true)]
    private ?int $imageSize = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $carousel0 = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $carousel1 = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $carousel2 = null;

    public function __construct()
    {
        $this->reservations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(float $prix): static
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * @return Collection<int, Reservation>
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): static
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations->add($reservation);
            $reservation->setForfait($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): static
    {
        if ($this->reservations->removeElement($reservation)) {
            // set the owning side to null (unless already changed)
            if ($reservation->getForfait() === $this) {
                $reservation->setForfait(null);
            }
        }

        return $this;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setImageFile(File $imageFile = null): void
    {
        $this->imageFile = $imageFile;
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $carouselFile0
     */
    public function setCarouselFile0(File $carouselFile0 = null): void
    {
        $this->carouselFile0 = $carouselFile0;
    }

    public function getCarouselFile0(): ?File
    {
        return $this->carouselFile0;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $carouselFile1
     */
    public function setCarouselFile1(File $carouselFile1 = null): void
    {
        $this->carouselFile1 = $carouselFile1;
    }

    public function getCarouselFile1(): ?File
    {
        return $this->carouselFile1;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $carouselFile2
     */
    public function setCarouselFile2(File $carouselFile2 = null): void
    {
        $this->carouselFile2 = $carouselFile2;
    }

    public function getCarouselFile2(): ?File
    {
        return $this->carouselFile2;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    public function setImageName(?string $imageName): static
    {
        $this->imageName = $imageName;

        return $this;
    }

    public function getImageSize(): ?int
    {
        return $this->imageSize;
    }

    public function setImageSize(?int $imageSize): static
    {
        $this->imageSize = $imageSize;

        return $this;
    }

    public function __toString()
    {
        return $this->getNom();
    }

    public function getCarousel0(): ?string
    {
        return $this->carousel0;
    }

    public function setCarousel0(string $carousel0): static
    {
        $this->carousel0 = $carousel0;

        return $this;
    }

    public function getCarousel1(): ?string
    {
        return $this->carousel1;
    }

    public function setCarousel1(string $carousel1): static
    {
        $this->carousel1 = $carousel1;

        return $this;
    }

    public function getCarousel2(): ?string
    {
        return $this->carousel2;
    }

    public function setCarousel2(?string $carousel2): static
    {
        $this->carousel2 = $carousel2;

        return $this;
    }
}
