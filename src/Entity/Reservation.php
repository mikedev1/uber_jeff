<?php

namespace App\Entity;

use App\Repository\ReservationRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ReservationRepository::class)]
class Reservation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'reservations', cascade: ['persist'])]
    private ?User $client = null;

    #[ORM\ManyToOne(inversedBy: 'reservations', cascade: ['persist'])]
    private ?Forfait $forfait = null;

    #[ORM\Column(nullable: true)]
    private ?float $prix = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $numero_de_facture = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTime $isCreatedAt = null;

    #[ORM\Column(nullable: true)]
    private ?bool $isPaid = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $depart = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $arriver = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTime $createdAt = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTime $retourAt = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClient(): ?User
    {
        return $this->client;
    }

    public function setClient(?User $client): static
    {
        $this->client = $client;

        return $this;
    }

    public function getForfait(): ?Forfait
    {
        return $this->forfait;
    }

    public function setForfait(?Forfait $forfait): static
    {
        $this->forfait = $forfait;

        return $this;
    }

    public function __toString()
    {
        return $this->getClient();
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(?float $prix): static
    {
        $this->prix = $prix;

        return $this;
    }

    public function getNumeroDeFacture(): ?string
    {
        return $this->numero_de_facture;
    }

    public function setNumeroDeFacture(?string $numero_de_facture): static
    {
        $this->numero_de_facture = $numero_de_facture;

        return $this;
    }

    public function getIsCreatedAt(): ?\DateTime
    {
        return $this->isCreatedAt;
    }

    public function setIsCreatedAt(?\DateTime $isCreatedAt): static
    {
        $this->isCreatedAt = $isCreatedAt;

        return $this;
    }

    public function isIsPaid(): ?bool
    {
        return $this->isPaid;
    }

    public function setIsPaid(?bool $isPaid): static
    {
        $this->isPaid = $isPaid;

        return $this;
    }

    public function getDepart(): ?string
    {
        return $this->depart;
    }

    public function setDepart(?string $depart): static
    {
        $this->depart = $depart;

        return $this;
    }

    public function getArriver(): ?string
    {
        return $this->arriver;
    }

    public function setArriver(?string $arriver): static
    {
        $this->arriver = $arriver;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTime $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getRetourAt(): ?\DateTime
    {
        return $this->retourAt;
    }

    public function setRetourAt(?\DateTime $retourAt): static
    {
        $this->retourAt = $retourAt;

        return $this;
    }
}
