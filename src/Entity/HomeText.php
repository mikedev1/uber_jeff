<?php

namespace App\Entity;

use App\Repository\HomeTextRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: HomeTextRepository::class)]
class HomeText
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $titre = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $subtitle = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $slogan = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $phraseAccroche = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $titreAccroche = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $subtitleAccroche = null;

    #[ORM\Column(length: 255)]
    private ?string $titreEn = null;

    #[ORM\Column(length: 255)]
    private ?string $titreEs = null;

    #[ORM\Column(length: 255)]
    private ?string $titreIt = null;

    #[ORM\Column(length: 255)]
    private ?string $subtitleEn = null;

    #[ORM\Column(length: 255)]
    private ?string $subtitleEs = null;

    #[ORM\Column(length: 255)]
    private ?string $subtitleIt = null;

    #[ORM\Column(length: 255)]
    private ?string $sloganEn = null;

    #[ORM\Column(length: 255)]
    private ?string $sloganEs = null;

    #[ORM\Column(length: 255)]
    private ?string $sloganIt = null;

    #[ORM\Column(length: 255)]
    private ?string $phraseAccrocheEn = null;

    #[ORM\Column(length: 255)]
    private ?string $phraseAccrocheEs = null;

    #[ORM\Column(length: 255)]
    private ?string $phraseAccrocheIt = null;

    #[ORM\Column(length: 255)]
    private ?string $titreAccrocheEn = null;

    #[ORM\Column(length: 255)]
    private ?string $titreAccrocheEs = null;

    #[ORM\Column(length: 255)]
    private ?string $titreAccrocheIt = null;

    #[ORM\Column(length: 255)]
    private ?string $subtitleAccrocheEn = null;

    #[ORM\Column(length: 255)]
    private ?string $subtitleAccrocheEs = null;

    #[ORM\Column(length: 255)]
    private ?string $subtitleAccrocheIt = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(?string $titre): static
    {
        $this->titre = $titre;

        return $this;
    }

    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    public function setSubtitle(?string $subtitle): static
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    public function getSlogan(): ?string
    {
        return $this->slogan;
    }

    public function setSlogan(?string $slogan): static
    {
        $this->slogan = $slogan;

        return $this;
    }

    public function getPhraseAccroche(): ?string
    {
        return $this->phraseAccroche;
    }

    public function setPhraseAccroche(?string $phraseAccroche): static
    {
        $this->phraseAccroche = $phraseAccroche;

        return $this;
    }

    public function getTitreAccroche(): ?string
    {
        return $this->titreAccroche;
    }

    public function setTitreAccroche(?string $titreAccroche): static
    {
        $this->titreAccroche = $titreAccroche;

        return $this;
    }

    public function getSubtitleAccroche(): ?string
    {
        return $this->subtitleAccroche;
    }

    public function setSubtitleAccroche(?string $subtitleAccroche): static
    {
        $this->subtitleAccroche = $subtitleAccroche;

        return $this;
    }

    public function getTitreEn(): ?string
    {
        return $this->titreEn;
    }

    public function setTitreEn(string $titreEn): static
    {
        $this->titreEn = $titreEn;

        return $this;
    }

    public function getTitreEs(): ?string
    {
        return $this->titreEs;
    }

    public function setTitreEs(string $titreEs): static
    {
        $this->titreEs = $titreEs;

        return $this;
    }

    public function getTitreIt(): ?string
    {
        return $this->titreIt;
    }

    public function setTitreIt(string $titreIt): static
    {
        $this->titreIt = $titreIt;

        return $this;
    }

    public function getSubtitleEn(): ?string
    {
        return $this->subtitleEn;
    }

    public function setSubtitleEn(string $subtitleEn): static
    {
        $this->subtitleEn = $subtitleEn;

        return $this;
    }

    public function getSubtitleEs(): ?string
    {
        return $this->subtitleEs;
    }

    public function setSubtitleEs(string $subtitleEs): static
    {
        $this->subtitleEs = $subtitleEs;

        return $this;
    }

    public function getSubtitleIt(): ?string
    {
        return $this->subtitleIt;
    }

    public function setSubtitleIt(string $subtitleIt): static
    {
        $this->subtitleIt = $subtitleIt;

        return $this;
    }

    public function getSloganEn(): ?string
    {
        return $this->sloganEn;
    }

    public function setSloganEn(string $sloganEn): static
    {
        $this->sloganEn = $sloganEn;

        return $this;
    }

    public function getSloganEs(): ?string
    {
        return $this->sloganEs;
    }

    public function setSloganEs(string $sloganEs): static
    {
        $this->sloganEs = $sloganEs;

        return $this;
    }

    public function getSloganIt(): ?string
    {
        return $this->sloganIt;
    }

    public function setSloganIt(string $sloganIt): static
    {
        $this->sloganIt = $sloganIt;

        return $this;
    }

    public function getPhraseAccrocheEn(): ?string
    {
        return $this->phraseAccrocheEn;
    }

    public function setPhraseAccrocheEn(string $phraseAccrocheEn): static
    {
        $this->phraseAccrocheEn = $phraseAccrocheEn;

        return $this;
    }

    public function getPhraseAccrocheEs(): ?string
    {
        return $this->phraseAccrocheEs;
    }

    public function setPhraseAccrocheEs(string $phraseAccrocheEs): static
    {
        $this->phraseAccrocheEs = $phraseAccrocheEs;

        return $this;
    }

    public function getPhraseAccrocheIt(): ?string
    {
        return $this->phraseAccrocheIt;
    }

    public function setPhraseAccrocheIt(string $phraseAccrocheIt): static
    {
        $this->phraseAccrocheIt = $phraseAccrocheIt;

        return $this;
    }

    public function getTitreAccrocheEn(): ?string
    {
        return $this->titreAccrocheEn;
    }

    public function setTitreAccrocheEn(string $titreAccrocheEn): static
    {
        $this->titreAccrocheEn = $titreAccrocheEn;

        return $this;
    }

    public function getTitreAccrocheEs(): ?string
    {
        return $this->titreAccrocheEs;
    }

    public function setTitreAccrocheEs(string $titreAccrocheEs): static
    {
        $this->titreAccrocheEs = $titreAccrocheEs;

        return $this;
    }

    public function getTitreAccrocheIt(): ?string
    {
        return $this->titreAccrocheIt;
    }

    public function setTitreAccrocheIt(string $titreAccrocheIt): static
    {
        $this->titreAccrocheIt = $titreAccrocheIt;

        return $this;
    }

    public function getSubtitleAccrocheEn(): ?string
    {
        return $this->subtitleAccrocheEn;
    }

    public function setSubtitleAccrocheEn(string $subtitleAccrocheEn): static
    {
        $this->subtitleAccrocheEn = $subtitleAccrocheEn;

        return $this;
    }

    public function getSubtitleAccrocheEs(): ?string
    {
        return $this->subtitleAccrocheEs;
    }

    public function setSubtitleAccrocheEs(string $subtitleAccrocheEs): static
    {
        $this->subtitleAccrocheEs = $subtitleAccrocheEs;

        return $this;
    }

    public function getSubtitleAccrocheIt(): ?string
    {
        return $this->subtitleAccrocheIt;
    }

    public function setSubtitleAccrocheIt(string $subtitleAccrocheIt): static
    {
        $this->subtitleAccrocheIt = $subtitleAccrocheIt;

        return $this;
    }
}
