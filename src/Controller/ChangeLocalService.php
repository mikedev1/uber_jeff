<?php


namespace App\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

final class ChangeLocalService extends AbstractController
{
    #[Route('/change-locale/{locale}', name: 'change_locale')]
    public function changeLocale ($locale, Request $request){

        //on stocke la langue demandée dans la session
        $request->getSession()->set('_locale', $locale);
        //on retourne sur la page précédente
        return $this->redirect($request->headers->get('referer'));
    }
}

