<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class ContactController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        private MailerService $mailerService,
        private TranslatorInterface $translator
    ) {
    }

    #[Route('/contact', name: 'app_contact')]
    public function index(
        Request $request
    ): Response {
        $contact = new Contact();

        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            dump($form->getData());
            $contact->setEmail($form->getData()->getEmail());
            $contact->setTelephone($form->getData()->getTelephone());
            $contact->setObjet($form->getData()->getObjet());
            $contact->setContenu($form->getData()->getContenu());
            $contact->setCreatedAt($form->getData()->getCreatedAt(new DateTimeImmutable('now')));

            $this->mailerService->sendEmail(
                $form->getData()->getEmail(),
                $form->getData()->getTelephone(),
                $form->getData()->getObjet(),
                $form->getData()->getContenu(),
            );

            $this->em->persist($contact);
            $this->em->flush();

            $message = $this->translator->trans('Votre message a bien été envoyé');
            $this->addFlash('success', $form->getData()->getEmail(). ' ' .$message);
        }

        return $this->render('contact/index.html.twig', [
            'contact' => $contact,
            'form' => $form
        ]);
    }
}
