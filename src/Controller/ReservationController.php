<?php

namespace App\Controller;

use App\Form\PrixAuKilometreType;
use App\Form\SmsType;
use App\Repository\ForfaitRepository;
use App\Repository\HomeTextRepository;
use App\Service\ContactService;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\ReservationRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Notifier\Message\SmsMessage;
use Symfony\Component\Notifier\TexterInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ReservationController extends AbstractController
{
    public function __construct(
        public EntityManagerInterface $em,
        public ForfaitRepository $forfaitRepository,
        public ContactService $contactService,
        public HomeTextRepository $homeText,
        public MailerService $mailer,
        private TranslatorInterface $translator
    ) {}
    public $form;


    #[Route('/', name: 'app_reservation')]
    public function index(
        TranslatorInterface $translator,
        Request $request,
        SessionInterface $session,
        TexterInterface $texter
    ): Response {

        $session->remove('reservation');


        $form = $this->contactService->getForm();

        //  $forfaits = $this->forfaitRepository->findAll();
        $forfaits = $this->forfaitRepository->findForfaits(['au kilomètre']);

        $forfaitKilometre = $this->forfaitRepository->findBy(['nom' => 'au kilomètre']);
        $texts = $this->homeText->findAll();

        //Traductions des elements en ase de données
        $smsForm = $this->createForm(SmsType::class);
        $smsForm->handleRequest($request);


        if ($smsForm->isSubmitted() && $smsForm->isValid()) {

            $dt = $smsForm->getData()['telephone'];

            $sms = new SmsMessage(
                '+33624364902',
                'Vous avez une nouvelle demande de ' . $dt . ' pour une course.',
                'BJVTC'
            );
            // dd($sms);

            $texter->send($sms);
            $message = $this->translator->trans('Votre demande a bien été envoyé. Vous recevrez un appel dans quelques minutes');
            $this->addFlash('success', $smsForm->getData()['telephone'] . ' ' . $message);
        }

        foreach ($texts as $text) {
            $translator->trans($text->getTitre());
            $translator->trans($text->getSubtitle());
            $translator->trans($text->getSlogan());
            $translator->trans($text->getPhraseAccroche());
            $translator->trans($text->getTitreAccroche());
            $translator->trans($text->getSubtitleAccroche());
        }

        //creation du formulaire pour le calcul du prix au kilometre
        $formKilometre = $this->createForm(PrixAuKilometreType::class);
        $formKilometre->handleRequest($request);
        /* Soumission du formulaire */
        if ($formKilometre->isSubmitted() && $formKilometre->isValid()) {
            $session->set('reservation', [
                'prix' => $formKilometre->getData()['prix'],
            ]);

            //dd($forfaitKilometre[0]->getId());

            return $this->redirectToRoute('app_panier', ['id' => $forfaitKilometre[0]->getId()]);
            dd($session->get('reservation'));
        };

        /* Data à utiliser pour la modale comment ça marche */

        $modalData = [
            [
                'image' => 'images/comment_ca_marche/ouverture.png',
                'image_alt' => 'ouverture du système de direction',
                'content' => "Veuillez cliquer sur l'icone panneau de direction"
            ],
            [
                'image' => 'images/comment_ca_marche/adresse.png',
                'image_alt' => 'Itinéraire',
                'content' => "Veuillez saisir votre adresse de départ et d'arrivée, puis cliquer sur le bouton 'Calculer'"
            ],
            [
                'image' => 'images/comment_ca_marche/tarif.png',
                'image_alt' => 'ouverture du système de direction',
                'content' => "Veuillez cliquer sur 'Afficher le tarif de ma course' puis sur 'Réserver en ligne'"
            ],

        ];

        return $this->render('reservation/index.html.twig', [
            'forfaits' => $forfaits,
            'homeTexts' => $texts,
            'forfaitKilometre' => $forfaitKilometre,
            'form' => $form->createView(),
            'formKilometre' => $formKilometre->createView(),
            'smsForm' => $smsForm->createView(),
            'modalData' => $modalData,
        ]);
    }

    #[Route('/mesreservations', name: 'app_mes_resa')]
    public function reservation(ReservationRepository $reservation)
    {
        $user = $this->getUser();

        return $this->render('reservation/mesreservations.html.twig', [
            'resas' => $reservation->findBy(
                ['client' => $user]
            ),
        ]);
    }
}
