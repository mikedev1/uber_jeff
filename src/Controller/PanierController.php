<?php

namespace App\Controller;

use App\Entity\User;
use Stripe\StripeClient;
use App\Entity\Reservation;
use App\Service\MailerService;
use App\Repository\UserRepository;
use App\Form\DetailReservationType;
use App\Repository\ForfaitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class PanierController extends AbstractController
{
    private $em;

    private $gateway;

    public function __construct(
        EntityManagerInterface $em,
        public UserRepository $userRepository,
        private ForfaitRepository $forfaitRepository,
        private MailerService $mailerService,
        private UserPasswordHasherInterface $userPasswordHasher,
        private EntityManagerInterface $entityManager
    ) {

        $this->em = $em;
        $this->gateway = new StripeClient($_ENV['STRIPE_SECRET_KEY_TEST']);
    }

    #[Route('/panier/{id}', name: 'app_panier')]
    public function index(
        SessionInterface $session,
        Request $request
    ): Response {
        $forfait = $this->forfaitRepository->findOneBy([
            'id' => $request->get('id'),
        ]);
        // Selection des photos du carousel
        $carousels = [
            $forfait->getCarousel0(),
            $forfait->getCarousel1(),
            $forfait->getCarousel2(),
        ];

        //prix au kilometre
     
            $prixAuKilometre = $session->get('reservation')['prix'] ?? null;
      dump($prixAuKilometre,  $session->get('reservation'));
        // Initialisation du panier
        $user = $this->getUser();
        $session->set('panier', [
            'user' => $this->getUser(),
            'forfait' => $forfait,
            'createdAt' => null,
            'prixFinal' => $prixAuKilometre ?? $forfait->getPrix(),
            'retourAt' => null,
            'email' => null,
            'telephone' => ''
        ]);

        // Formulaire concernant le nombre de passagers et de valises
        $form = $this->createForm(DetailReservationType::class);
        $form->handleRequest($request);

        /* Creation du formulaire de reservation de course */
        if ($form->isSubmitted() && $form->isValid()) {

            if (false == $form->getData()['retour']) {
                $prixFinal = $session->get('panier')['prixFinal'];
            } else {
                $prixFinal = $session->get('panier')['prixFinal'] * 2 * 0.9;
            }
            $session->set('panier', [
                'user' => $session->get('panier')['user'],
                'forfait' => $forfait,
                'createdAt' => $form->getData()['createdAt'],
                'prixFinal' => $prixFinal,
                'retourAt' => $form->getData()['retourAt'],
                'email' => $form->getData()['email'],
                'telephone' => $form->getData()['telephone'],
            ]);

            

            if ($this->userRepository->findByEmail($form->getData()['email']) && !$this->getUser()) {
                $this->addFlash('info', ('Le mail' . ' ' . $form->getData()['email'] . ' ' . 'est déjà utilisé'));
                return $this->redirect($this->generateUrl($request->get('_route'), ['id' => $forfait->getId()]));
            };

            return $this->redirectToRoute('app_checkout');
        }

        //dd($session->get('panier')['prixFinal']);

        return $this->render('panier/index.html.twig', [
            'user' => $user,
            'panier' => $session->get('panier'),
            'form' => $form->createView(),
            'forfait' => $forfait,
            'carousels' => $carousels,
            'prixAuKilometre' => $prixAuKilometre,
        ]);
    }

    #[Route('/checkout', name: 'app_checkout')]
    public function checkout(
        SessionInterface $session
    ): Response {
        $prix = $session->get('panier')['prixFinal'] * 100;
        $nomDuForfait = $session->get('panier')['forfait']->getNom();

        $checkout = $this->gateway->checkout->sessions->create(
            [
                // 'payment_method_types' => ['card'],
                'line_items' => [
                    [
                        'price_data' => [
                            'currency' => 'eur',
                            'unit_amount' => intval($prix),
                            'product_data' => [
                                'name' => $nomDuForfait,
                            ],
                        ],
                        'quantity' => 1,
                    ],
                ],
                'mode' => 'payment',
                'success_url' => 'http://127.0.0.1:8000/checkout/success?stripeSessionId={CHECKOUT_SESSION_ID}',
                'cancel_url' => 'http://127.0.0.1:8000/checkout/cancel?stripeSessionId={CHECKOUT_SESSION_ID}',
            ]
        );

        return $this->redirect($checkout->url);
    }

    #[Route('/checkout/success', name: 'app_checkout_success')]
    public function checkoutSuccess(Request $request, SessionInterface $session): Response
    {
        if (null == $session->get('panier')) {
            return $this->redirectToRoute('app_mes_resa');
        }
        // récuperation des donnees concernant le paiement de l'utilisateur
        $idSessions = $request->query->get('stripeSessionId');

        $stripe = $this->gateway->checkout->sessions->retrieve(
            $idSessions,
            []
        );

        $panier = $session->get('panier');

        $reservation = new Reservation();

        if ('complete' == $stripe['status'] && 'paid' == $stripe['payment_status']) {
            $reservation->setIsPaid(true);
        } else {
            $reservation->setIsPaid(false);
        }

        if ($this->getUser()) {
            $user = $this->getUser();
        } else {
            /* Creation d un mot de passe aleatoire */
            function genererChaineAleatoire($longueur, $listeCar = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
            {
                $chaine = '';
                $max = mb_strlen($listeCar, '8bit') - 1;
                for ($i = 0; $i < $longueur; ++$i) {
                    $chaine .= $listeCar[random_int(0, $max)];
                }
                return $chaine;
            }
            $user = new User();
            $user
                ->setEmail($panier['email'])
                ->setTelephone($panier['telephone'])
                ->setRoles(['ROLE_USER'])
                ->setPassword(
                    $this->userPasswordHasher->hashPassword(
                        $user,
                        genererChaineAleatoire(20)
                    )
                );
            $this->em->persist($user);
            $this->em->flush();
        }

        $forfait = $this->forfaitRepository->findOneBy(['id' => $panier['forfait']->getId()]);
        $session->remove('panier');
        $reservation
            ->setNumeroDeFacture($stripe['id'])
            ->setClient($user)
            ->setForfait($forfait)
            ->setCreatedAt($panier['createdAt'])
            ->setPrix($stripe['amount_total'] / 100)
            ->setIsCreatedAt(new \DateTime('now'))
            ->setRetourAt($panier['retourAt']);
        $this->em->persist($reservation);
        $this->em->flush();

        if ($reservation->getRetourAt()) {
            $retour =  'Le retour est prévu pour le ' . $reservation->getRetourAt()->format('d/m/Y à H:i:s');
        } else {
            $retour = '';
        }

        $this->mailerService->reservationEmail(
            $panier['email'],
            'Réservation',
            'Réservation de' . $panier['email'] . ' est enregistrée pour le forfait "' . $forfait->getNom() . '" le ' . $reservation->getCreatedAt()->format('d/m/Y à H:i:s') . $retour
        );

        return $this->render('panier/success.html.twig', [
            'forfait' => $forfait,
        ]);
    }

    #[Route('/checkout/cancel', name: 'app_checkout_cancel')]
    public function checkoutCancel(Request $request): Response
    {
        return $this->render('panier/cancel.html.twig', []);
    }
}
