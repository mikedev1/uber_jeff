<?php

namespace App\Controller\Admin;

use App\Entity\HomeText;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class HomeTextCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return HomeText::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            //IdField::new('id'),
            TextField::new('titre')->setLabel('Titre en français'),
            TextField::new('titreEn')->setLabel('Titre en anglais')->onlyWhenUpdating(),
            TextField::new('titreEs')->setLabel('Titre en espagnol')->onlyWhenUpdating(),
            TextField::new('titreIt')->setLabel('Titre en italien')->onlyWhenUpdating(),

            TextEditorField::new('subtitle')->setLabel('Sous titre en français'),
            TextEditorField::new('subtitleEn')->setLabel('Sous titre en anglais')->onlyWhenUpdating(),
            TextEditorField::new('subtitleEs')->setLabel('Sous titre en espagnol')->onlyWhenUpdating(),
            TextEditorField::new('subtitleIt')->setLabel('Sous titre en italien')->onlyWhenUpdating(),

            TextEditorField::new('slogan')->setLabel('Slogan en français')->onlyWhenUpdating(),
            TextEditorField::new('sloganEn')->setLabel('Slogan en anglais')->onlyWhenUpdating(),
            TextEditorField::new('sloganEs')->setLabel('Slogan en espagnol')->onlyWhenUpdating(),
            TextEditorField::new('sloganIt')->setLabel('Slogan en italien')->onlyWhenUpdating(),

            TextEditorField::new('phraseAccroche')->setLabel('Phrase d\'accroche en français')->onlyWhenUpdating(),
            TextEditorField::new('phraseAccrocheEn')->setLabel('Phrase d\'accroche en anglais')->onlyWhenUpdating(),
            TextEditorField::new('phraseAccrocheEs')->setLabel('Phrase d\'accroche en espagnol')->onlyWhenUpdating(),
            TextEditorField::new('phraseAccrocheIt')->setLabel('Phrase d\'accroche en italien')->onlyWhenUpdating(),

            TextField::new('titreAccroche')->setLabel('Titre d\'accroche en français')->onlyWhenUpdating(),
            TextField::new('titreAccrocheEn')->setLabel('Titre d\'accroche en anglais')->onlyWhenUpdating(),
            TextField::new('titreAccrocheEs')->setLabel('Titre d\'accroche en espagnol')->onlyWhenUpdating(),
            TextField::new('titreAccrocheIt')->setLabel('Titre d\'accroche en italien')->onlyWhenUpdating(),

            TextEditorField::new('subtitleAccroche')->setLabel('Sous titre en français')->onlyWhenUpdating(),
            TextEditorField::new('subtitleAccrocheEn')->setLabel('Sous titre en anglais')->onlyWhenUpdating(),
            TextEditorField::new('subtitleAccrocheEs')->setLabel('Sous titre en espagnol')->onlyWhenUpdating(),
            TextEditorField::new('subtitleAccrocheIt')->setLabel('Sous titre en italien')->onlyWhenUpdating(),


        ];
    }
   
}
