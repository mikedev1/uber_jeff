<?php

namespace App\Controller\Admin;

use App\Entity\Forfait;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ForfaitCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Forfait::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('nom'),
            NumberField::new('prix'),
            TextField::new('imageFile')->setFormType(VichImageType::class)->hideOnIndex(),
            ImageField::new('imageName')->setBasePath('/images/forfaits')->onlyOnIndex(),

            TextField::new('carouselFile0')->setFormType(VichImageType::class)->hideOnIndex(),
            ImageField::new('carousel0')->setBasePath('/images/forfaits')->onlyOnIndex(),

            TextField::new('carouselFile1')->setFormType(VichImageType::class)->hideOnIndex(),
            ImageField::new('carousel1')->setBasePath('/images/forfaits')->onlyOnIndex(),

            TextField::new('carouselFile2')->setFormType(VichImageType::class)->hideOnIndex(),
            ImageField::new('carousel2')->setBasePath('/images/forfaits')->onlyOnIndex(),
        ];
    }
}
