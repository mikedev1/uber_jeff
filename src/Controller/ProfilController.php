<?php

namespace App\Controller;

use App\Form\ProfileType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProfilController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em
    ) {
    }
    #[Route('/profil', name: 'app_profil')]
    public function index(
        Request $request,
    ): Response {
        $form = $this->createForm(ProfileType::class);
        $form->handleRequest($request);

        $user = $this->getUser();
        if ($form->isSubmitted() && $form->isValid()) {
            $user
            ->setNom($form->getData()->getNom())
            ->setPreNom($form->getData()->getPrenom())
            ->setPays($form->getData()->getPays() ?? $user->getPays())
            ->setTelephone($form->getData()->getTelephone());
            dump($form->getData());
            
            $this->em->persist($user);
            $this->em->flush();
            $this->addFlash('info', 'Votre profil a bien été modifié');
        }

        return $this->render('profil/index.html.twig', [
            'form' => $form
        ]);
    }
}
