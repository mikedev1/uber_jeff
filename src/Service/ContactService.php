<?php

namespace App\Service;

use App\Entity\Commentaire;
use App\Form\CommentaireType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;

class ContactService extends AbstractController
{
    public function __construct(
        private CommentaireType $commentaireType,
        private RequestStack $requestStack,
        private EntityManagerInterface $em
    ) {
    }

    public function getForm()
    {
        $request = $this->requestStack->getCurrentRequest();
        $commentaire = new Commentaire();
        $form = $this->createForm(CommentaireType::class, $commentaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $commentaire->setCreatedAt(new \DateTimeImmutable('now'));
            dd($form->getdata());
            $this->em->persist($commentaire);
            $this->em->flush();
        }

        return $form;
    }
}
