<?php

namespace App\Service;

use App\Entity\Forfait;

final class StripeService
{
    private $privateKey;

    public function __construct()
    {
        if ('dev' == $_ENV['APP_ENV']) {
            $this->privateKey = $_ENV['STRIPE_SECRET_KEY_TEST'];
        } else {
            $this->privateKey = $_ENV['STRIPE_SECRET_KEY_LIVE'];
        }
    }

    public function paymentIntent(Forfait $forfait)
    {
        //  \Stipe\Stripe::setApiKey($this->privateKey);
        $client = new \Stripe\StripeClient($this->privateKey);

        $intent = $client->paymentIntents->create([
            'amount' => $forfait->getPrix() * 100,
            'currency' => 'eur',
            'confirm' => true,
            'payment_method' => [
                'card',
            ],
        ]);

        return $intent;
    }

    public function paiement(
        $amount,
        $currency,
        $description,
        array $stripeParameter
    ) {
        /* @var \Stripe\StripeClient $client */
        \Stripe\Stripe::setApiKey($this->privateKey);

        $paiement_intent = null;

        if (isset($stripeParameter['stripeIntentId'])) {
            $paiement_intent = \Stripe\PaymentIntent::retrieve($stripeParameter['stripeIntentId']);
        }
        if ('succeeded' == $stripeParameter['stripeIntentId']) {
            // TODO
        } else {
            $paiement_intent->cancel();
        }

        return $paiement_intent;
    }

    public function stripe(array $stripeParameter, Forfait $forfait)
    {
        return $this->paiement(
            $forfait->getPrix() * 100,
            'eur',
            $forfait->getNom(),
            $stripeParameter
        );
    }
}
