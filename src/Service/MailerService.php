<?php

// src/Controller/MailerController.php

namespace App\Service;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class MailerService extends AbstractController
{
    public function __construct(
        private MailerInterface $mailer,
    ) {
    }

    #[Route('/email')]
    public function sendEmail(
        $emailClient,
        $telephone,
        $objet,
        $content
    ): void {
        $contentMail = '<h1> Salut Jeff</h1> <p>'.$emailClient.' vient de vous envoyer un message</p> <p>'.$content.', il est joignable au '.$telephone;

        $email = (new Email())
            ->from($emailClient)
            ->to('contact@trans-air-beauvais.com')
            // ->cc('uber.jeff.95@gmail.com')
            // ->cc($emailClient)
            ->replyTo($emailClient)
            ->priority(Email::PRIORITY_HIGH)
            ->subject($objet)
            ->text('')
            ->html($contentMail);

        $emailForClient = (new Email())
            ->from('contact@trans-air-beauvais.com')
            ->to($emailClient)
            // ->cc('uber.jeff.95@gmail.com')
            // ->cc($emailClient)
            ->replyTo('contact@trans-air-beauvais.com')
            ->priority(Email::PRIORITY_HIGH)
            ->subject($objet)
            ->text('')
            ->html('Le message avec le contenu suvant "'.$content.'" a bien été envoyé');

        $this->mailer->send($email);
        $this->mailer->send($emailForClient);
    }

    public function reservationEmail($emailClient, $objet, $content): void
    {
        $email = (new Email())
            ->from($emailClient)
            ->to('contact@trans-air-beauvais.com')
            // ->cc('cc@example.com')
            // ->bcc('bcc@example.com')
            // ->replyTo('fabien@example.com')
            ->priority(Email::PRIORITY_HIGH)
            ->subject($objet)
            ->text('')
            ->html($content);

        $this->mailer->send($email);
    }
}
