

// initialisation de la carte
var carte = L.map("maCarte").setView([49.455098, 2.112679], 11);
// chargement des "tuiles"
L.tileLayer("https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png", {
  attribution:
    '© <a href="//osm.org/copyright">OpenstreetMap</a> contributors ',
  minZoom: 1,
  maxZoom: 20,
}).addTo(carte);

// ajouter un marqueur
// var marqueur = L.marker([49.455098, 2.112679]).addTo(carte);
// marqueur.bindPopup("Aéroport Paris Beauvais");
var lyrMaps = L.geoportalLayer.WMTS(
  {
    layer: "GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2",
  },
  {
    // leafletParams
    opacity: 0.7,
  }
);
carte.addLayer(lyrMaps);
var routeCtrl = L.geoportalControl.Route({});
carte.addControl(routeCtrl);

// addEventListener('click', distance);

// function distance(e) {
//   console.log(document.querySelector('.GProuteResultsValue>div').textContent);
//   let distance = document.querySelector('.GProuteResultsValue>div').textContent;
//   document.querySelector('input#distance').value = distance
// }





// L.Routing.control({
//   routeWhileDragging: true,
//   geocoder: L.Control.Geocoder.nominatim(),
// }).addTo(carte);

