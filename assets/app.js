/*
 * Welcome to your app's main Java
Script file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)

// start the Stimulus application
//import './bootstrap';
import "@fortawesome/fontawesome-free/css/all.css";
import "@fortawesome/fontawesome-free/js/all";
import "tw-elements";
import "./styles/app.scss";
import "./scripts/scrollToTop";
import "./bootstrap";
// Initialization for ES Users
import { initTE, SmoothScroll, Collapse, Dropdown, Input, Ripple, Datepicker, Timepicker, Select, Modal, Datetimepicker, Carousel, Clipboard, Alert } from "tw-elements";

import AOS from 'aos';
import 'aos/dist/aos.css'; // You can also use <link> for styles
// ..
AOS.init({
  duration: 1000,
  easing: 'ease-in-out-cubic',
});

initTE({ SmoothScroll, Collapse, Dropdown, Input, Ripple, Datepicker, Timepicker, Select, Modal, Carousel, Datetimepicker, Clipboard, Alert });

import flatpickr from "flatpickr";
flatpickr("#createdAt", {});

const myExample = document.getElementById("myExample");
const alertInstance = Alert.getInstance(
  document.getElementById("container-example")
);

myExample.addEventListener("copy.te.clipboard", () => {
  myExample.innerText = "Copié!";
  alertInstance.show();

  setTimeout(() => {
    myExample.innerText = "✔";
  }, 4000);
});

const myMail = document.getElementById("myMail");
const alertMail = Alert.getInstance(
  document.getElementById("container-mail")
);

myMail.addEventListener("copy.te.clipboard", () => {
  myMail.innerText = "Copié!";
  alertMail.show();

  setTimeout(() => {
    myMail.innerText = "✔";
  }, 4000);
});

function toggleModal() { document.getElementById('modal').classList.toggle('hidden')
}

toggleModal();









